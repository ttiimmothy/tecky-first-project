let wrongmessage = document.querySelector(".show-unmatch-password");
let firstpassword = document.querySelector(".passwordinput-firstpassword");
let password = document.querySelector(".passwordinput-password");
let resetPassword = document.querySelector("#reset-password-form");
let username = document.querySelector(".username");
let firstFullDisplay = document.querySelector(".first-full-display");
let secondFullDisplay = document.querySelector(".second-full-display");
let timeSecond = document.querySelector(".second");
let resetMessage = document.querySelector(".reset-message")

async function showUsername() {
    const search = new URLSearchParams(location.search);
    const name = search.get("name");
    username.innerText = name.replace("+", " ");
}

firstpassword.addEventListener('input',function(){
    wrongmessage.style = "color:#ff0000"
    if(firstpassword.value !== password.value){
        wrongmessage.innerText = "Password is unmatched"
    }else if(firstpassword.value === password.value){
        wrongmessage.innerText = "Password is matched"
        wrongmessage.style = "color:#0011ff"
    }
    if(!(password.value) || !(firstpassword.value)){
        wrongmessage.innerText = '';
    }
})

password.addEventListener('input',function(){
    wrongmessage.style = "color:#ff0000"
    if(firstpassword.value !== password.value){
        wrongmessage.innerText = "Password is unmatched"
    }else if(firstpassword.value === password.value){
        wrongmessage.innerText = "Password is matched"
        wrongmessage.style = "color:#0011ff"
    }
    if(!(password.value) || !(firstpassword.value)){
        wrongmessage.innerText = '';
    }
})

resetPassword.addEventListener("submit",async function(event){
    event.preventDefault();
    const form = event.currentTarget;
    let user = {};
    user.name = username.innerText;
    user.password = form.password.value;

    let res = await fetch('/get-user');
    const checkUsername = await res.json();

    for(let checkUser of checkUsername){
        if(checkUser.name === user.name){
            if(firstpassword.value === password.value && firstpassword.value){
                firstFullDisplay.classList.remove("show-first-full-display");
                secondFullDisplay.classList.add("show-second-full-display");
                resetMessage.innerText = `${username.innerText}, your password is reset`
                changeTime();
                await fetch('/reset-password',{
                    method:"POST",
                    headers:{
                        "Content-Type":"application/json"
                    },
                    body:JSON.stringify(user)
                });
            }
        }
    }
})

function changeTime(){
    let sec = 5;
    timeSecond.innerText = sec;
    let intervalID = setInterval(function(){
        if(sec > 0){
            sec--;
            timeSecond.innerText = sec;
        }else{
            clearInterval(intervalID);
            window.location = "/";
        }
    },1000);
}

window.onload = async function(){
    firstFullDisplay.classList.add("show-first-full-display");
    await showUsername();
}