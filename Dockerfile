FROM node:lts
WORKDIR /usr/src/app
COPY package.json .
RUN yarn install
COPY . .
EXPOSE 8080
CMD yarn knex migrate:latest --env production &&\
node index.js