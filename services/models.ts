export interface User{
    id: number;
    name: string;
    password: string;
    is_in_room: boolean;
    is_ready: boolean;
    is_playing: boolean;
    player_order: number;
    email: string;
    is_host: boolean;
    icon: string;
    birthday: Date;
    experience: string;
}
export interface Friend{
    id: number;
}
export interface Result{
    name:string,
    score:number
}
export interface Player{
    id:number,
    score:Score,
    resource:Resource,
    deck : PlayerDeck,
    holdDeck :PlayerHoldDeck,
    novelCardDeck:NovelCard[]
    buyCard:(bank:BankResource,card:Card)=>void
    getResource:(bank:BankResource,resource:Resource)=>boolean
    holdCard:(card:Card)=>boolean
    novelCardCheck:()=>boolean
}
interface PlayerDeck{
    card:Card[]

    add:(card:Card)=>void
    totalProduction:()=>Resource
}
interface PlayerHoldDeck{
    cardHold:Card[]

    add:()=>void
    remove:()=>void
    checkCardStorage:()=>number
}
interface BankResource{
    resource:Resource
    add:(resourceReturn:Resource)=>void
    reduce:(resourceProvide:Resource)=>void
    checkStorageEnough:(resourceProvide:Resource)=>boolean
}
interface Card{
    tier:string,
    id:number,
    score:Score,
    production:Resource,
    cost:Resource
}
interface NovelCard{
    id:number,
    score:Score,
    productionRequirement:Resource
}
interface Resource{
    red?:number,
    green?:number,
    blue?:number,
    brown?:number,
    white?:number,
    gold?:number,
}
type Score = number
export interface GameTable{
    novelCardArray:NovelCard[],
    highLevelDeck:Card[],
    midLevelDeck:Card[],
    lowLevelDeck:Card[],
    highLevelShownDeck:Card[],
    midLevelShownDeck:Card[],
    lowLevelShownDeck:Card[],
    gameStart:()=>void
}