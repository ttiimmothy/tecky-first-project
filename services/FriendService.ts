import {Client} from "pg";

export class FriendService{
	constructor(private client:Client){}

	async getFriendId(username:string){
		return (await this.client.query(/*sql*/`select id from users where name = $1`,[username])).rows[0];
	}

	async checkFriend(userId:number,friendId:number){
		return (await this.client.query(/*sql*/`select * from friends where user_id = $1
		and another_user_id = $2`,[userId,friendId])).rows[0];
	}

	async getUserId(name:string){
		return (await this.client.query(/*sql*/`select id,name from users where name = $1`,
		[name])).rows[0];
	}

	async addFriend(userId:number,friendId:number){
		await this.client.query(/*sql*/`insert into friends(user_id,another_user_id) values
		($1,$2),($3,$4)`,[userId,friendId,friendId,userId]);
	}

	async removeFriend(userId:number,friendId:number){
		await this.client.query(/*sql*/`delete from friends where user_id = $1 and another_user_id = $2`,
		[userId,friendId]);
		await this.client.query(/*sql*/`delete from friends where user_id = $1 and another_user_id = $2`,
		[friendId,userId]);
	}

	async loadFriend(userId:number){
		return (await this.client.query(/*sql*/`select name from users inner join friends on another_user_id = users.id
		where user_id = $1`,[userId])).rows;
	}
}