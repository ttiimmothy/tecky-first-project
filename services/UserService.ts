import {Client} from "pg";

export class UserService{
	constructor(private client:Client){}

	async loginInfos(){
		return (await this.client.query(/*sql*/`select * from users`)).rows
	}

	async loginInfosByEmail(email:string){
		return (await this.client.query(/*sql*/`select * from users where users.email = $1`,[email])).rows
	}

	async createSocialUsers(name:string,password:string,email:string,icon:string){
		await this.client.query(/*sql*/`insert into users(name,password,email,icon,birthday,degree)
		values ($1,$2,$3,$4,'1990-01-01','')`,[name,password,email,icon])
	}

	async loginInfosByName(name:string){
		return (await this.client.query(/*sql*/`select * from users where users.name = $1`,[name])).rows
	}

	async createSocialUsersWithoutIcon(name:string,password:string){
		await this.client.query(/*sql*/`insert into users(name,password,email,icon,birthday,degree)
		values ($1,$2,'','','1990-01-01','')`,[name,password])
	}

	async createSocialUsersWithoutEmail(name:string,password:string,icon:string){
		await this.client.query(/*sql*/`insert into users (name,password,email,icon,birthday,degree)
		values ($1,$2,'',$3,'1990-01-01','')`,[name,password,icon])
	}

	async register(username:string,password:string,email:string,icon:string|undefined,birthday:string|undefined,
	degree:string|undefined){
		await this.client.query(/*sql*/`insert into users(name,password,email,icon,birthday,degree)
		values ($1,$2,$3,$4,$5,$6)`,[
			username,
			password,
			email,
			icon ? icon : '',
			birthday ? birthday : '1990-01-01',
			degree ? degree : '',
		])
	}

	async createGuest(username:string,password:string){
		await this.client.query(/*sql*/`insert into users(name,password,email,icon,birthday,degree)
		values ($1,$2,'','','1990-01-01','Beginner')`,[username,password]);
	}

	async orderLoginInfos(){
		return (await this.client.query(/*sql*/`select * from users order by id`)).rows;
	}

	async checkPassword(name:string){
		return (await this.client.query(/*sql*/`select password from users where name = $1`,[name]))
		.rows[0].password;
	}

	async updateUsername(username:string,id:number){
		await this.client.query(/*sql*/`update users set name = $1 where id = $2`,[username,id]);
	}

	async updatePassword(password:string,id:number){
		await this.client.query(/*sql*/`update users set password = $1 where id = $2`,[password,id]);
	}

	async updateUserInfo(email:string,icon:string|undefined,birthday:string|undefined,degree:string|undefined,
	skype:string|undefined,bio:string|undefined,id:number){
		await this.client.query(/*sql*/`update users set email = $1,icon = $2,birthday = $3,degree = $4,skype = $5,
		bio = $6 where id = $7`,
		[email,icon ? icon : '',birthday ? birthday : '1990-01-01',degree ? degree : '',skype ? skype : '',
		bio ? bio : '',id]);
	}

	async resetPassword(password:string,name:string){
		await this.client.query(/*sql*/`update users set password = $1 where name = $2`,[password,name]);
	}
}