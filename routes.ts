import express from "express";
import {isLoggedInAPI} from "./guards";
import {adminController, friendController, historyController,messageController,roomController,soundController,
upload,userController} from "./main";

export const roomRoutes = express.Router();
export const userRoutes = express.Router();
export const historyRoutes = express.Router();
export const messageRoutes = express.Router();
export const soundRoutes = express.Router();
export const friendRoutes = express.Router();
export const adminRoutes = express.Router();

export function initRoutes(){
	roomRoutes.get("/create-room",isLoggedInAPI,roomController.createRoom);
	roomRoutes.get("/room/:uuId",isLoggedInAPI,roomController.joinRoom);
	roomRoutes.get("/quit_room",isLoggedInAPI,roomController.quitRoom);

	userRoutes.post('/login',userController.login);
	userRoutes.get('/login/google',userController.googleLogin);
	userRoutes.get('/login/facebook',userController.facebookLogin);
	userRoutes.get('/login/github',userController.githubLogin)
	userRoutes.get('/get-user',userController.allUsers);
	userRoutes.post('/signup',upload.single('icon'),userController.register);
	userRoutes.get('/current-user',isLoggedInAPI,userController.checkCurrentUser);
	userRoutes.get('/current-user-information',isLoggedInAPI,userController.currentUserInfo);
	userRoutes.get('/guest',userController.guest);
	userRoutes.post('/forget-password',userController.forgetPasswordEmail);
	userRoutes.post('/users',isLoggedInAPI,userController.user)
	userRoutes.get('/logout',userController.logout);
	userRoutes.post('/check-password',userController.checkPassword);
	userRoutes.post('/update-profile',upload.single('icon'),userController.updateProfile);
	userRoutes.post('/reset-password',userController.resetPassword)

	historyRoutes.post('/history',isLoggedInAPI,historyController.history)
	historyRoutes.post('/result',historyController.result);
	historyRoutes.get('/all-history',isLoggedInAPI,historyController.allHistory)

	messageRoutes.post('/transmission',messageController.transmission)
	messageRoutes.post('/message/1',messageController.guestMessage)
	messageRoutes.post('/message/2',messageController.userMessage)
	messageRoutes.post('/message/3',messageController.roomMessage)
	messageRoutes.post("/message/4",messageController.friendMessage);
	messageRoutes.get('/chat-record-global',messageController.loadChatGlobal)
	messageRoutes.get('/chat-record-user',messageController.loadChatUser)
	messageRoutes.get('/chat-record-room',messageController.loadChatRoom)
	messageRoutes.get("/chat-record-friend",messageController.loadChatFriend);
	messageRoutes.post('/store-chat-friend',messageController.storeChatFriend);
	messageRoutes.post('/store-voice-friend',upload.single("content"),messageController.storeVoiceFriend)

	soundRoutes.get('/sound',soundController.sound)
	soundRoutes.get('/lobby-sound',soundController.soundLobby)

	friendRoutes.post('/add-friend',friendController.addFriend)
	friendRoutes.post('/remove-friend',friendController.removeFriend)
	friendRoutes.get('/load-friend',friendController.loadFriend)

	adminRoutes.post('/add-question',adminController.addQuestion)
	adminRoutes.get('/load-question',adminController.loadQuestion)
	adminRoutes.post('/answer-question',adminController.answerQuestion)
}