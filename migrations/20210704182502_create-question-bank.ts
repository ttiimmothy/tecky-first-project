import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("question_bank",(table) => {
		table.increments();
		table.string("name");
		table.string("question");
		table.string("answer");
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("question_bank");
}