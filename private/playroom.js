import {bank,gameTable} from "./table.js";
import {lowCardArr,midCardArr,highCardArr,novelCardDeck} from "./card.js";
import {Player} from "./player.js";
import {shuffle} from "./lib.js";
import {addHoldCardOn} from "./holdCard.js";
import {buyShownDeckCardOn} from "./buyCard.js";
import {playerCardDetailsCreate,playerHoldCardDetailsCreate} from "./playerDetail.js";
import {resetGetToken} from "./getResource.js";
import {getSound} from "./sound.js";
import {stopRecorder} from "./gameCapture.js"
import {messagePanel1,messagePanel2,messagePanel3,messagePanel4,userprofile} from "./chatroom.js";

// global variable ;
export let playerOrder;
export let currentTurn;
export let currentPlayer; // starting
export let numberOfPlayer;
export let playOrderArray;
export let enterOrderArray;
export let gameIsStart = false;
export let isYourTurn = false;
export let playerArray = [];
export let isReconnect = false
export const saveGameButton = document.querySelector(".save-game-button")

let startTime;
let endTime;
let meetScoreTarget = false;

// socket.io
export let socket;
export function initSocket() {
    socket = io.connect();
    socket.on("global-chatroom", function (data) {
        let li = document.createElement("li")
        li.innerText = data;
        messagePanel1.appendChild(li);

    })
    socket.on("user-chatroom", function (data) {
        // console.log(data);
        let li = document.createElement("li")
        li.innerText = data;
        messagePanel2.appendChild(li);
    })
    socket.on("room-chatroom", function (data) {
        let li = document.createElement("li")
        li.innerText = data;
        messagePanel3.appendChild(li);
    })
    socket.on("friend-chatroom", async function (data) {
        // console.log(data);
        let li = document.createElement("li")
        li.innerText = data.message;
        messagePanel4.appendChild(li);

        let message = {};
        message.name = data.name;
        message.content = data.content;
        message.belong = userprofile.innerText;
        await fetch("/store-chat-friend", {
            method:"POST",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify(message)
        })
    })
}
initSocket();

socket.on("new-player", playerData => { //playerData : {}
	// console.log(playerData)
    numberOfPlayer = playerData.length
    enterOrderArray = []
    for (let i = 0; i < playerData.length; ++i) {
        enterOrderArray.push(playerData[i].username)
        document.querySelectorAll(".player-username")[i].innerText = playerData[i].username;
        document.querySelectorAll(".player-container-left .status")[i].innerText = "waiting..."
    }
    if (playerData.length == 1) {
        document.querySelector(".ready-button").disabled = true
    } else {
        document.querySelector(".ready-button").disabled = false
    }
})
socket.on("quit-room", playerData => {
    // console.log(playerData)
    enterOrderArray = []
    if (!gameIsStart) {
        for (let i = 0; i < 4; ++i) {
            document.querySelectorAll(".player-username")[i].innerText = ""
            document.querySelectorAll(".player-container-left .status")[i].innerText = ""
        }
        for (let i = 0; i < playerData.length; ++i) {
            enterOrderArray.push(playerData[i].username)
            document.querySelectorAll(".player-username")[i].innerText = playerData[i].username;
            document.querySelectorAll(".player-container-left .status")[i].innerText = "waiting..."
        }
        if (playerData.length == 1) {
            document.querySelector(".ready-button").disabled = true
        }
    }
})
socket.on("player-quit", username => { //for the room is started
	if(playerArray){
		const index = playerArray.findIndex(item => item.name == username)
		document.querySelectorAll(".player-username")[index].innerText = `${username} ("QUIT")`;
	}
})
socket.on("ready-player", readyPlayer => {//readyPlayer : string
    // console.log(enterOrderArray)
    // console.log(readyPlayer)
    const readyPlayerIndex = enterOrderArray.findIndex(item => item == readyPlayer)
    // console.log("Ready", readyPlayerIndex)
    document.querySelectorAll(".player-container-left .status")[readyPlayerIndex].innerText = "Ready!!"
})
socket.on("cancel-ready-player", cancelPlayer => {//readyPlayer : string
    const cancelReadyPlayerIndex = enterOrderArray.findIndex(item => item == cancelPlayer)
    // console.log("Cancel Ready", cancelReadyPlayerIndex)
    document.querySelectorAll(".player-container-left .status")[cancelReadyPlayerIndex].innerText = "waiting..."
})
socket.on("playerOrder", (data) => {
    playerOrder = data
    playerHoldCardDetailsCreate(data);
});

socket.on("get-token-effect", async (data) => { //data : {}
    await getSound("getToken", "effect") //get token sound effect
    let playerToken = document.querySelectorAll(`#player${data.player + 1} .my-token`)
    // console.log(playerToken)
    for (let token in Object.keys(data.token)) {
        let colorToken = Object.keys(data.token)[token]
        // console.log(data.token[colorToken])
        if (data.token[colorToken] > 0) {
            let tokenTag = playerToken[token]
            tokenTag.classList.add("animate__animated")
            tokenTag.classList.add("animate__bounce")
        }
        // console.log(data)
    }
})

socket.on("hold-card-effect", async (data) => {//data : {}
    await getSound("holdCard", "effect")
    let playerHoldCard = data.player
    let holdTag = document.querySelector(`#player${playerHoldCard + 1} .hold`)
    holdTag.classList.add("animate__animated")
    holdTag.classList.add("animate__rubberBand")
    let draggedCardType = data.cardHoldType
    let cardHoldEffect;
    // console.log(draggedCardType)
    switch (draggedCardType.tier) {
        case "HL":
            cardHoldEffect = document.querySelectorAll(".high-level>div")[draggedCardType.shownCardArrayIndex + 1]

            break;
        case "ML":
            cardHoldEffect = document.querySelectorAll(".mid-level>div")[draggedCardType.shownCardArrayIndex + 1]

            break;
        case "LL":
            cardHoldEffect = document.querySelectorAll(".low-level>div")[draggedCardType.shownCardArrayIndex + 1]

            break;
    }
    // console.log(cardHoldEffect)
    cardHoldEffect.classList.add("animate__animated")
    cardHoldEffect.classList.add("animate__rubberBand")
})

socket.on("buy-card-effect", async (data) => {//data : {}
    await getSound("holdCard", "effect")
    await getSound("getToken", "effect")
    // console.log(data)
    let playerBuyCard = data.player
    let detailTag = document.querySelector(`#player${playerBuyCard + 1} .detail`)
    detailTag.classList.add("animate__animated")
    detailTag.classList.add("animate__rubberBand")
    if (data.buyFrom == "table") {
        let cardBuyType = data.cardBuyType
        let cardBuyEffect;
        // console.log(cardBuyType)
        switch (cardBuyType.tier) {
            case "HL":
                cardBuyEffect = document.querySelectorAll(".high-level>div")[cardBuyType.shownCardArrayIndex + 1]
                break;
            case "ML":
                cardBuyEffect = document.querySelectorAll(".mid-level>div")[cardBuyType.shownCardArrayIndex + 1]

                break;
            case "LL":
                cardBuyEffect = document.querySelectorAll(".low-level>div")[cardBuyType.shownCardArrayIndex + 1]

                break;
        }
    }
})

socket.on("get-novel-card-effect", async (data) => {//data : {}
    await getSound("holdCard", "effect")
    let novelCardIndex = data.novelCardIndex
    // console.log(novelCardIndex)
    let novelCardEffect = document.querySelectorAll("#novel-card-container .novel-card")[novelCardIndex]
    novelCardEffect.classList.add("animate__animated")
    novelCardEffect.classList.add("animate__bounceOut")
})


socket.on("game-host-assigned",gameHost);

socket.on("you-may-start", () => {
    startGameButton.style.display = "block"
    startGameButton.classList.remove("animate__flipOutX")
    startGameButton.classList.add("animate__flipInX")
});
socket.on("you-may-not-start", () => {
    startGameButton.classList.remove("animate__flipInX")
    startGameButton.classList.add("animate__flipOutX")
});

socket.on("game-started", async (initInformation) => {
    startTime = new Date()

    saveGameButton.disabled = false;
    startGameButton.classList.remove("animate__flipInX")
    startGameButton.classList.add("animate__bounceOut")
    const readyButton = document.querySelector(".ready-button");
    readyButton.classList.add("animate__bounceOut")

    //To all player
    // console.log(initInformation)
    numberOfPlayer = initInformation.numOfPlayers
    currentTurn = initInformation.currentPlayer
    playOrderArray = initInformation.playerOrderArray
    currentPlayer = (currentTurn - 1) % numberOfPlayer;
    // console.log("number of Player : ", numberOfPlayer);
    // console.log("current turn : ", currentTurn);
    // console.log("player order : ", playOrderArray)
    playerArray = []
    playOrderArray.forEach((item, index) => {
        document.querySelectorAll(".player-username")[index].innerText = item;
        playerArray.push(new Player(item))
    })

    buyShownDeckCardOn();
    playerCardDetailsCreate();
    if (!isReconnect) {
        gameInit();
    }

    // console.log(currentPlayer)
    document.querySelectorAll(".player-information")[currentPlayer].classList.add("your-turn")
});


socket.on("first-player", (initInformation) => {
    //To first Player
    isYourTurn = true;
    // console.log(isYourTurn);
    numberOfPlayer = initInformation.numOfPlayers
    currentTurn = initInformation.currentPlayer
    currentPlayer = (currentTurn - 1) % numberOfPlayer;
    startTurn();
});
socket.on("your-turn", (turnInformation) => {
    //To current turn  player
    isYourTurn = true;
    // console.log("start turn : ", isYourTurn);
    currentTurn = turnInformation;
    currentPlayer = (currentTurn - 1) % numberOfPlayer;
    startTurn();
});
socket.on("current-turn", (turnInformation) => {
    //To all player
    // console.log(playerArray[currentPlayer].deck);
    currentTurn = turnInformation;
    currentPlayer = (currentTurn - 1) % numberOfPlayer;
    // console.log("Current Game turn  : ", currentTurn);
    if (currentPlayer == 0) {
        document.querySelectorAll(".player-information")[currentPlayer + numberOfPlayer - 1].classList.remove("your-turn")
    } else {
        document.querySelectorAll(".player-information")[currentPlayer - 1].classList.remove("your-turn")
    }
    document.querySelectorAll(".player-information")[currentPlayer].classList.add("your-turn")
});

socket.on("remaining-turn", (remainingTurn) => {
    meetScoreTarget = true
    document.querySelector(".game-status").innerText = `Remaining ${remainingTurn} turns`;
});
socket.on("end-game", async () => {
    endTime = new Date()
    document.querySelector(".game-status").innerText = "";

    const timeSpent = endTime.getMinutes() - startTime.getMinutes()
    let playerScoreArray = []
    let playerScore = { name: "", score: 0 }
    let winner = { name: "", score: 0 };
    for (let i = 0; i < numberOfPlayer; ++i) {
        playerScore.name = playerArray[i].name;
        playerScore.score = playerArray[i].score;
        playerScoreArray.push({ "name": playerArray[i].name, "score": playerArray[i].score })

        if (playerArray[i].score >= winner.score) {
            winner.score = playerArray[i].score;
            winner.name = playerArray[i].name;
        }
    }

    playerScoreArray.sort((a, b) => b.score - a.score)
    // console.log(playerScoreArray)

    let endGameResult = {}
    endGameResult["timeSpent"] = timeSpent
    endGameResult["result"] = playerScoreArray
    await fetch("/result", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: JSON.stringify(endGameResult)
    })

    //show result
    await getSound("champion", "effect")
    let resultTable = document.querySelector("#result-table")
    resultTable.style.display = "block"
    resultTable.classList.add("animate__animated")
    resultTable.classList.add("animate__zoomIn")

    resultTable.innerHTML = `<div id="result-top-bar">
        <h1>
            Rank Win
        </h1>
        <p>
        Total Time : ${timeSpent} min
        </p>
    </div>`
    // ${(index+1==1)?`${<img src="/image/crown.png" alt="crown"/>}`:""}
    playerScoreArray.forEach((item, index) => {
        resultTable.innerHTML += `<div class="row row-item rank${index + 1}">
            <div class="col-2 column-item result-rank">
                ${index + 1}
            </div>
            <div class="col-7 column-item result-name">
                ${item.name}
            </div>
            <div class="col-3 column-item result-score">
                ${item.score}
            </div>
        </div>`
    })

    socket.emit("game-rank", playerScoreArray);
    stopRecorder();
})

socket.on("reconnect", () => {
    isReconnect = true
    gameIsStart = true
})

// Game button
export const startGameButton = document.querySelector("#start-game");
startGameButton.addEventListener("click", startGame);
async function startGame() {
    document.querySelector(".game-host").innerText = "";
    socket.emit("start-game");
    await getSound("openingSound", "bgm")
}

export const readyButton = document.querySelector(".ready-button");
readyButton.addEventListener("click", ready);
function ready(e) {
    if (e.target.textContent === "Cancel") {
        e.target.textContent = "Ready";
        socket.emit("cancel-ready");
    } else {
        e.target.textContent = "Cancel";
        socket.emit("ready");
    }
}

// Game function
function gameHost(){
    document.querySelector(".game-host").innerText = /*html*/`You are the game host. Please wait for others to ready.`;
}

function startTurn() {
    // console.log("change turn");
    // console.log("261 : ", playerArray)
    // console.log("262 : ", currentPlayer)
    document.querySelector("#table-container .window-block").style.zIndex = -1;
    document.querySelector("#token-bank .window-block").style.zIndex = -1;
    document.querySelectorAll(".player-username")[currentPlayer].innerText = `${playerArray[currentPlayer].name} (Your Turn)`;
    document.querySelectorAll(".finger-image")[currentPlayer].style.height = "50px"
    addHoldCardOn();

    //enable hold card
    //other logic to be added
}

export function endTurn() {
    isYourTurn = false;
    document.querySelectorAll(".player-username")[currentPlayer].innerText = `${playerArray[currentPlayer].name}`;
    document.querySelectorAll(".finger-image")[currentPlayer].style.height = "0px"
    // console.log("end turn : ", isYourTurn);
    if (playerArray[currentPlayer].checkGameEnd()) {
        // document.querySelector(`#player${currentPlayer+1} .player-score`).classList.add("meetScoreClass")
        meetScoreTarget = true;
    }
    socket.emit("end-turn", meetScoreTarget); // what the player has done to be added as data
    resetGetToken();

    document.querySelector("#table-container .window-block").style.zIndex = 2;
    document.querySelector("#token-bank .window-block").style.zIndex = 2;
    //other logic to be added
}
//will be triggered by socket.io  click->socket->socket back

async function gameInit() {
    //remove status
    document.querySelectorAll(".player-container-left .status").forEach(item => item.innerText = "")
    gameIsStart = true;

    // console.log("create table deck");
    gameTable.novelCardDeck = JSON.parse(JSON.stringify(shuffle(novelCardDeck)));
    gameTable.highLevelDeck = JSON.parse(JSON.stringify(shuffle(highCardArr)));
    gameTable.midLevelDeck = JSON.parse(JSON.stringify(shuffle(midCardArr)));
    gameTable.lowLevelDeck = JSON.parse(JSON.stringify(shuffle(lowCardArr)));

    for(let i = 0; i < 5; ++i){
        gameTable.novelShownCard.push(gameTable.novelCardDeck.pop());
    }
    for (let i = 0; i < 4; ++i) {
        gameTable.highLevelShownDeck.push(gameTable.highLevelDeck.pop());
        gameTable.midLevelShownDeck.push(gameTable.midLevelDeck.pop());
        gameTable.lowLevelShownDeck.push(gameTable.lowLevelDeck.pop());
    }
    // console.log("original HL shown deck", gameTable.highLevelShownDeck);
    // console.log("original HL deck", gameTable.highLevelDeck);

    await dataTransfer();
}
export async function dataTransfer(){
    const formObject = {};
    formObject["gameTable"] = gameTable;
    formObject["bankResource"] = bank;
    formObject["playerOrder"] = playerOrder;
    for (let i = 0; i < numberOfPlayer; ++i) {
        formObject[`player${i}`] = playerArray[i];
    }
    await fetch("/transmission", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(formObject),
    })
}

socket.on("transmission-received",(data)=>{
    gameTable.novelCardDeck = data["gameTable"].novelCardDeck;
    gameTable.novelShownCard = data["gameTable"].novelShownCard;
    gameTable.highLevelDeck = data["gameTable"].highLevelDeck;
    gameTable.highLevelShownDeck = data["gameTable"].highLevelShownDeck;
    gameTable.midLevelDeck = data["gameTable"].midLevelDeck;
    gameTable.midLevelShownDeck = data["gameTable"].midLevelShownDeck;
    gameTable.lowLevelDeck = data["gameTable"].lowLevelDeck;
    gameTable.lowLevelShownDeck = data["gameTable"].lowLevelShownDeck;
    bank.resource = data["bankResource"].resource;
    for (let i = 0; i < numberOfPlayer; i++) {
        playerArray[i].resource = data[`player${i}`].resource;
        playerArray[i].score = data[`player${i}`].score;
        playerArray[i].deck = data[`player${i}`].deck;
        playerArray[i].holdDeck = data[`player${i}`].holdDeck;
        playerArray[i].novelCardDeck = data[`player${i}`].novelCardDeck;
    }
    statusUpdate();
});

async function statusUpdate() {
    if (currentTurn == 1 || isReconnect) {
        //click start and first player
        const tableContainerWidth = document.querySelector("#table-container").clientWidth;
        const playerContainerWidth = document.querySelector("#player-container").clientWidth;
        // console.log(playerContainerWidth)
        const w = (tableContainerWidth - 600) / 4 + 120
        console.log("update status change");
        document.querySelectorAll(".c1").forEach((item) => {
            item.style.left = `${item.offsetLeft}px`
            // console.log(item.offsetLeft)
            item.style.left = `${1 * w}px`
            item.style.zIndex = "1";
        });
        document.querySelectorAll(".c2").forEach((item) => {
            item.style.left = `${item.offsetLeft}px`
            item.style.left = `${2 * w}px`
            item.style.zIndex = "1";
        });
        document.querySelectorAll(".c3").forEach((item) => {
            item.style.left = `${item.offsetLeft}px`
            item.style.left = `${3 * w}px`
            item.style.zIndex = "1";
        });
        document.querySelectorAll(".c4").forEach((item) => {
            item.style.left = `${item.offsetLeft}px`
            item.style.left = `${4 * w}px`
            item.style.zIndex = "1";
        });

        setTimeout(()=>{
            document.querySelectorAll(".level-card ").forEach(item => { item.style.justifyContent = "space-between" })
            document.querySelectorAll(".shown-card ").forEach(item => { item.style.position = "unset" })
        },1500)
        await getSound("cardDispatch","effect")
        for (let i = 0; i < numberOfPlayer; ++i) {
            let playerCard = document.querySelectorAll(`#player${i + 1} .player-container-right div`)

            playerCard[0].innerText = "HOLD"
            playerCard[1].innerText = "BOUGHT"
            playerCard[0].classList.add("hold")
            playerCard[1].classList.add("detail")
        }
        // mainly due to reconnect
        let novelCadContainer = document.querySelector("#novel-card-container");
        novelCadContainer.innerHTML = "";
        gameTable.novelShownCard.forEach(() => {
            let novelCard = document.createElement("div");
            novelCard.classList.add("novel-card");
            novelCadContainer.appendChild(novelCard);
            isReconnect = false;
        })

        // document.querySelector("body").removeChild(document.querySelector("#main-button-window"))
    }

    // table deck number of card
    document.querySelector(".high-level-deck div").innerText =
        "Remaining Card : " + gameTable.highLevelDeck.length;
    document.querySelector(".mid-level-deck div").innerText =
        "Remaining Card : " + gameTable.midLevelDeck.length;
    document.querySelector(".low-level-deck div").innerText =
        "Remaining Card : " + gameTable.lowLevelDeck.length;

    const levelShownDeck = document.querySelectorAll(".level-card");
    levelShownDeck.forEach((cardList, cardLevel) => {
        cardList.querySelectorAll(".shown-card").forEach((cardElement, index) => {
            let gameDeck;
            if(cardLevel === 0){
                gameDeck = gameTable.highLevelShownDeck[index];
            }else if(cardLevel === 1){
                gameDeck = gameTable.midLevelShownDeck[index];
            }else{
                gameDeck = gameTable.lowLevelShownDeck[index];
            }

            if(!gameDeck){
                cardElement.parentNode.removeChild(cardElement);
                return;
            }
            let costItemDiv = "";
            for(let key in gameDeck.cost){
                if(gameDeck.cost[key] !== 0){
                    costItemDiv += /*html*/`
                    <div class="${key} cost-circle">
                        ${gameDeck.cost[key]}
                    </div>`;
                }
            }
            let productionImg;
            if(gameDeck.production === "red"){
                productionImg = /*html*/`<img src="/image/ruby2.png" class="img-fluid" alt="ruby">`;
            }else if(gameDeck.production === "green"){
                productionImg = /*html*/`<img src="/image/jade.png" class="img-fluid jade-image" alt="jade">`;
            }else if(gameDeck.production === "blue"){
                productionImg = /*html*/`<img src="/image/topaz2.png" class="img-fluid " alt="topaz">`;
            }else if(gameDeck.production === "brown"){
                productionImg = /*html*/`<img src="/image/obsidian2.png" class="img-fluid" alt="obsidian">`;
            }else{
                productionImg = /*html*/`<img src="/image/diamond.png" class="img-fluid" alt="diamond">`;
            }
            cardElement.innerHTML = /*html*/`
            <div class="${gameDeck.production} card-cover">
                <div class="card-top">
                    <div class="score">${gameDeck.score}</div>
                    <div class="production">
                        ${productionImg}
                    </div>
                </div>
                <div class="card-bottom">
                    <div class="cost">
                        ${costItemDiv}
                    </div>
                    <div>
                        ${gameDeck.id}
                    </div>
                </div>
            </div>`;
        })
    })

    // novel are shown novel card assign card id to card element
    let novelCardElement = document.querySelectorAll(".novel-card");
    // console.log(gameTable.novelShownCard);
    novelCardElement.forEach((item, index) => {
        if (!gameTable.novelShownCard[index]) {
            novelCardElement[index].parentNode.removeChild(novelCardElement[index]);
            return;
        }
        let productionRequirement = "";
        for (let key in gameTable.novelShownCard[index].productionRequirement) {
            productionRequirement += `
            <div class="production-requirement ${key}">
                ${gameTable.novelShownCard[index].productionRequirement[key]}
            </div>`;
        }
        item.innerHTML = /*html*/`
		<div class="novel-card-left-bar">
			<div class="novel-card-score-container">
				<div class="novel-card-score">
					${gameTable.novelShownCard[index].score}
				</div>
			</div>
			<div class="production-requirement-container">
				${productionRequirement}
			</div>
		</div>
		<div>
			${gameTable.novelShownCard[index].id}
		</div>`;
    })

    // show bank remaining resource
    document
        .querySelectorAll(".bank-token .remaining-token")
        .forEach((item, index) => {
            // (item.parentNode).parentNode.querySelector(".virtual-token").innerText=""
            item.innerText = bank.resource[Object.keys(bank.resource)[index]];
        });

    // player 1-4 token
    for (let i = 0; i < numberOfPlayer; ++i) {
        document.querySelector(`#player${i + 1} .my-token-container`).innerHTML = /*html*/`
		<div class="my-token red">
			<div class="player-inner-token inner-circle">${playerArray[i].resource["red"]}</div>
		</div>
		<div class="my-token green">
			<div class="player-inner-token inner-circle">${playerArray[i].resource["green"]}</div>
		</div>
		<div class="my-token blue">
			<div class="player-inner-token inner-circle">${playerArray[i].resource["blue"]}</div>
		</div>
		<div class="my-token brown">
			<div class="player-inner-token inner-circle">${playerArray[i].resource["brown"]}</div>
		</div>
		<div class="my-token white">
			<div class="player-inner-token inner-circle">${playerArray[i].resource["white"]}</div>
		</div>
		<div class="my-token gold">
			<div class="player-inner-token inner-circle">${playerArray[i].resource["gold"]}</div>
		</div>`;
    }

    //player1-4 production
    for (let i = 0; i < numberOfPlayer; ++i) {
        const playerTotalProductionObj = playerArray[i].totalProduction();

        document.querySelector(`#player${i + 1} .my-production-container`).innerHTML = /*html*/`
		<div class="my-production">
		<img src="/image/ruby2.png" class="img-fluid" alt="ruby">
		${playerTotalProductionObj["red"]}</div>

		<div class="my-production">
		<img src="/image/jade.png" class="img-fluid jade-image" alt="jade">
		${playerTotalProductionObj["green"]}</div>

		<div class="my-production">
		<img src="/image/topaz2.png" class="img-fluid " alt="topaz">
		${playerTotalProductionObj["blue"]}</div>

		<div class="my-production">
		<img src="/image/obsidian2.png" class="img-fluid" alt="obsidian">
		${playerTotalProductionObj["brown"]}</div>

		<div class="my-production ">
		<img src="/image/diamond.png" class="img-fluid" alt="diamond">
		${playerTotalProductionObj["white"]}</div>`;
    }

    // player 1-4 score player-score
    for (let i = 0; i < numberOfPlayer; ++i) {
        document
            .querySelectorAll(`#player${i + 1} .player-score`)
            .forEach((item, index) => {
                item.innerText = `score : ${playerArray[i].score}`;
            });
    }
    // hold card update
    const holdCardElement = document.querySelectorAll(".hold");
    for (let i = 0; i < numberOfPlayer; ++i){
        holdCardElement[i].innerText = /*html*/`Hold : ${playerArray[i].holdDeck.length}`;
    }
}
