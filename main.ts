import express, {Request, Response, NextFunction} from 'express';
import expressSession from 'express-session';
import path from 'path';
import http from 'http';
import {Server as SocketIO, Socket} from 'socket.io';
import {Client} from 'pg';
import grant from 'grant';
import dotenv from 'dotenv';
import multer from 'multer';
import {v4 as uuid} from 'uuid';
import {RoomService} from "./services/RoomService";
import {RoomController} from "./controllers/RoomController";
import {adminRoutes,friendRoutes,historyRoutes,initRoutes,messageRoutes,roomRoutes,soundRoutes,
userRoutes} from "./routes";
import {UserService} from "./services/UserService";
import {UserController} from "./controllers/UserController";
import {HistoryService} from "./services/HistoryService";
import {HistoryController} from "./controllers/HistoryController";
import {MessageService} from "./services/MessageService";
import {MessageController} from "./controllers/MessageController";
import {SoundController} from "./controllers/SoundController";
import {FriendService} from "./services/FriendService";
import {FriendController} from "./controllers/FriendController";
import {AdminService} from "./services/AdminService";
import {AdminController} from "./controllers/AdminController";

const storage = multer.diskStorage({
    destination(req,file,cb) {
        cb(null, path.resolve('./public/uploads'));
    },
    filename(req,file,cb) {
        cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1] === 'octet-stream' ?
		'mp3' : file.mimetype.split('/')[1]}`);
    }
})
export const upload = multer({storage});

dotenv.config();
export const client = new Client({
	host:process.env.POSTGRES_HOST,
    database:process.env.POSTGRES_DB,
    user:process.env.POSTGRES_USER,
    password:process.env.POSTGRES_PASSWORD,
	// port:5432
	// database:process.env.DB_NAME,
    // user:process.env.DB_USERNAME,
    // password:process.env.DB_PASSWORD,
})
client.connect((error) => {
    if(error){
        console.log(error);
    }
})

const app = express();
const server = new http.Server(app);
export const io = new SocketIO(server);

app.use(express.urlencoded({extended: true}));
app.use(express.json());

const sessionMiddleware = expressSession({
    secret:"Splendor",
    resave:true,
    saveUninitialized:true,
    cookie:{secure:false},
})

app.use(sessionMiddleware);
io.use((socket:Socket,next:NextFunction) => {
    const request = socket.request as express.Request;
    sessionMiddleware(request, request.res as express.Response, next as express.NextFunction);
})

const grantExpress = grant.express({
    defaults: {
        // origin:process.env.WEBSITE_LINK,
		origin:process.env.WEBSITE_LINK,
        transport:"session",
        state:true,
    },
    google: {
        key: process.env.GOOGLE_CLIENT_ID || "",
        secret: process.env.GOOGLE_CLIENT_SECRET || "",
        scope: ['profile','email'],
        callback: '/login/google',
    },
    facebook: {
        key: process.env.FACEBOOK_CLIENT_ID || "",
        secret: process.env.FACEBOOK_CLIENT_SECRET || "",
        scope: ['openid','email'],
        callback: '/login/facebook',
    },
    github: {
        key: process.env.GITHUB_CLIENT_ID || "",
        secret: process.env.GITHUB_CLIENT_SECRET || "",
        scope: ['profile','email'],
        callback: '/login/github',
    }
})

app.use(grantExpress as express.RequestHandler);

io.on("connection",async(socket:Socket) => {
    const roomData = await allRoomData();
	// console.log(roomData)
    socket.emit('room-information',roomData);

    if((socket.request as express.Request).session["user"] && (socket.request as express.Request).session["loggedIn"]){
        socket.join('user');
        socket.join(`friend-${(socket.request as express.Request).session["user"].name}`);
    }

    if((socket.request as express.Request).session["user"]){
        const username = (socket.request as express.Request).session["user"].name;
        socket.join(username);

        const userId = (await client.query(/*sql*/`select id from users where name = $1`,
		[(socket.request as express.Request).session["user"].name])).rows[0];
        const friends = (await client.query(/*sql*/`select name from users inner join friends on
		another_user_id = users.id where user_id = $1`, [userId.id])).rows;

        for(const friend of friends){
            socket.join(`friend-${friend.name}`);
        }

        const isHost = (await client.query(/*sql*/`select is_host from users where users.name = $1`,[username]))
		.rows[0].is_host;
		// console.log(isHost)
		// console.log(username,isHost)
        const isPlaying = (await client.query(/*sql*/`select is_playing from users where users.name = $1`,[username]))
		.rows[0].is_playing;
        const isInRoom = (await client.query(/*sql*/`select is_in_room from users where users.name = $1`,[username]))
		.rows[0].is_in_room;

        if(isHost === true && !isPlaying){
			// console.log(username,isHost)
            io.in(username).emit("game-host-assigned");
        }

        if(isPlaying && !isInRoom){
            const uuId = (await client.query(/*sql*/`select rooms.uuid from users
				left outer join user_room_mapping on users.id = user_room_mapping.user_id
				left outer join rooms on rooms.id = user_room_mapping.room_id
				where users.name = $1`,
				[username]
			)).rows[0].uuid;

            const userGameData = {isPlaying,isInRoom,uuId};
            socket.emit("user-game-information", userGameData);
        }
    }

    if((socket.request as express.Request).session["uuId"]){
        const uuId = (socket.request as express.Request).session["uuId"];
        const doesRoomExist = (await client.query(/*sql*/`select * from rooms where uuid = $1`,[uuId])).rows[0];
        if(doesRoomExist){
            socket.join(uuId);
        }

        socket.on('ready', async () => {
            const username = (socket.request as express.Request).session["user"].name;
            const uuId = (socket.request as express.Request).session["uuId"];
            await client.query(/*sql*/`update rooms set num_of_ready_players = num_of_ready_players + 1 where uuid = $1`,
			[uuId]);
            await client.query(/* sql */ `update users set is_ready = $1 where name = $2`, [true,username]);
            const doesRoomExist = (await client.query(/*sql*/`select * from rooms where uuid = $1`, [uuId])).rows[0];
            if(doesRoomExist){
                const numOfPlayers = (await client.query(/*sql*/`select num_of_players from rooms where uuid = $1`,
				[uuId])).rows[0].num_of_players;
                const numOfReadyPlayers = (await client.query(/*sql*/`select num_of_ready_players from rooms
				where uuid = $1`,[uuId])).rows[0].num_of_ready_players;

                if (numOfPlayers === numOfReadyPlayers && numOfPlayers > 1) {
                    const host = (await client.query(/*sql*/`select users.name from rooms left outer join user_room_mapping on
						rooms.id = user_room_mapping.room_id
						left outer join users on users.id = user_room_mapping.user_id where is_host = $1
						and uuid = $2`,[true,uuId]
					)).rows[0].name;
                    io.to(host).emit('you-may-start');
                }
                // SAMUEL NEW ADD
                io.to(`${uuId}`).emit('ready-player', username);
            }
        })

        socket.on('cancel-ready', async () => {
            const username = (socket.request as express.Request).session["user"].name;
            const uuId = (socket.request as express.Request).session["uuId"];
            const doesRoomExist = (await client.query(/*sql*/`select * from rooms where uuid = $1`, [uuId])).rows[0];
            if (doesRoomExist) {
                await client.query(/*sql*/`update users set is_ready = false where name = $1`, [username]);
                await client.query(/*sql*/`update rooms set num_of_ready_players = num_of_ready_players - 1
				where uuid = $1`,[uuId]);
                const host = (await client.query(/*sql*/`select users.name from rooms left outer join user_room_mapping
					on rooms.id = user_room_mapping.room_id
					left outer join users on users.id = user_room_mapping.user_id where is_host = $1
					and uuid = $2`,[true,uuId]
				)).rows[0].name;
                io.to(host).emit('you-may-not-start');
                io.to(`${uuId}`).emit('cancel-ready-player', username);
            }
        });

        socket.on('start-game', async () => {
            const username = (socket.request as express.Request).session["user"].name;
            const uuId = (socket.request as express.Request).session["uuId"];
            const numOfPlayers = (await client.query(/*sql*/`select num_of_players from rooms where uuid = $1`,
			[uuId])).rows[0].num_of_players;
            if (numOfPlayers > 1) {
                const numOfReadyPlayers = (await client.query(/*sql*/`select num_of_ready_players from rooms
				where uuid = $1`,
				[uuId])).rows[0].num_of_ready_players;
                const currentTurn = (await client.query(/*sql*/`select current_turn from rooms where uuid = $1`,
				[uuId])).rows[0].current_turn;
                const host = (await client.query(/*sql*/`select users.name from rooms left outer join user_room_mapping
					on rooms.id = user_room_mapping.room_id
					left outer join users on users.id = user_room_mapping.user_id where is_host = $1
					and uuid = $2`,[true,uuId]
				)).rows[0].name;
                if (host == username && numOfPlayers === numOfReadyPlayers) {
                    await client.query(/*sql*/`update rooms set is_started = $1 where uuid = $2`, [true,uuId]);
                    await client.query(/*sql*/`update rooms set current_turn = current_turn + 1
					where uuid = $1`, [uuId]);

                    // change to await instead of promise dot then
                    const playerOrder = [];
                    const firstPlayer = await determineFirstPlayer(uuId, numOfPlayers);
                    for (let i = 0; i < numOfPlayers; i++) {
                        const newLocal = /*sql*/`select users.name from rooms left outer join user_room_mapping
						on rooms.id = user_room_mapping.room_id
                        left outer join users on users.id = user_room_mapping.user_id where  uuid = $1
						and player_order = $2`;
                        const user = (await client.query(/* sql */ newLocal, [uuId, i])).rows[0].name;
                        await client.query(/* sql */`update users set is_playing = $1 where name = $2`,
						[true,user]);
                        playerOrder.push(user);

                        io.to(user).emit('playerOrder', i);
                    }

                    await client.query(/* sql*/`update rooms set playerarray = $1 where uuid = $2`, [playerOrder, uuId]);
                    io.to(uuId).emit('game-started', {numOfPlayers, currentPlayer: currentTurn + 1,
					playerOrderArray: playerOrder});

                    io.to(firstPlayer).emit('first-player',{numOfPlayers,currentPlayer: currentTurn + 1});
                }
                // eslint-disable-next-line no-shadow
                const roomData = await allRoomData();
                io.emit('room-information', roomData);
            }
        });
        socket.on('get-token',(virtualToken) => {
            const uuId = (socket.request as express.Request).session["uuId"];
            io.to(`${uuId}`).emit('get-token-effect',virtualToken);
        });

        socket.on('hold-card',(data) => {
            const uuId = (socket.request as express.Request).session["uuId"];
            io.to(`${uuId}`).emit('hold-card-effect',data);
        });
        socket.on('buy-card',(data) => {
            const uuId = (socket.request as express.Request).session["uuId"];
            io.to(`${uuId}`).emit('buy-card-effect',data);
        });
        socket.on('get-novel-card',(data) => {
            const uuId = (socket.request as express.Request).session["uuId"];
            io.to(`${uuId}`).emit('get-novel-card-effect', data);
        });

        socket.on('game-rank',async(data) => {
            const username = (socket.request as express.Request).session["user"].name;
            const rank = data.findIndex((item: any) => item.name === username);
            const userExp = (await client.query(/*sql*/`select experience from users where name = $1`,
			[username])).rows[0].experience;
            let expIncrement;

            expIncrement = userExp;
            switch (rank) {
                case 0:
                    expIncrement += 30;
                    await client.query(/*sql*/`update users set experience = ${expIncrement} where name = $1`,
					[username]);
                    break;
                case 1:
                    expIncrement += 20;
                    await client.query(/*sql*/`update users set experience = ${expIncrement} where name = $1`,
					[username]);
                    break;
                case 2:
                    expIncrement += 10;
                    await client.query(/*sql*/`update users set experience = ${expIncrement} where name = $1`,
					[username]);
                    break;
                case 3:
                    expIncrement += 0;
                    await client.query(/*sql*/`update users set experience = ${expIncrement} where name = $1`,
					[username]);
                    break;
            }
        })

        socket.on('end-turn', async (meetScoreTarget) => {
            const username = (socket.request as express.Request).session["user"].name;
            const uuId = (socket.request as express.Request).session["uuId"];
            const currentTurn = (await client.query(/*sql*/`select current_turn from rooms where uuid = $1`,
			[uuId])).rows[0].current_turn;
            const playerOrder = (await client.query(/*sql*/`select player_order from users where users.name = $1`,
			[username])).rows[0].player_order;
            const numOfPlayers = (await client.query(/*sql*/`select num_of_players from rooms where uuid = $1`,
			[uuId])).rows[0].num_of_players;
            const whichPlayer = (await client.query(/*sql*/`select which_player from rooms where uuid = $1`,
			[uuId])).rows[0].which_player;
            const quitPlayerArray = (await client.query(/*sql*/`select quit_player from rooms where uuid = $1`,
			[uuId])).rows[0].quit_player;
            const numOfQuitPlayers = quitPlayerArray.length;
            const originalNumOfPlayers = numOfPlayers + numOfQuitPlayers;

            if(meetScoreTarget){
                const remainingTurn = originalNumOfPlayers - playerOrder - 1;
                if(remainingTurn === 0){
                    await client.query(/*sql*/`update rooms set is_started = $1 where uuid = $2`,[true,uuId]);
                    await client.query(/*sql*/`update rooms set is_ended = $1 where uuid = $2`,[true,uuId]);
                    io.to(uuId).emit('end-game','EndGame');
					// Don't change this two order , need call determineNextPlayer first
                }else{
                    if(whichPlayer === playerOrder){
                        endTurn(uuId,currentTurn,originalNumOfPlayers,quitPlayerArray);
                    }
                    io.to(uuId).emit("remaining-turn",remainingTurn);
					// Don't change this two order,need call determineNextPlayer first
                }
            }else if(whichPlayer === playerOrder){
                endTurn(uuId,currentTurn,originalNumOfPlayers,quitPlayerArray);
            }
        });

        socket.on('save-game-request',(data) => {
            const username = data.username;
            socket.join(`${username}`);
            const uuId = (socket.request as express.Request).session["uuId"];
            socket.to(`${uuId}`).emit('save-request', username);
        });
        socket.on('save-game-recieve', (data) => {
            // const username = data.username
            io.to(`${uuId}`).emit('player-response', data.result);
        });
        socket.on('all-player-agree-save', async () => {
            const uuId = (socket.request as express.Request).session["uuId"];
            const numOfPlayers = (await client.query(/* sql */`select num_of_players from rooms where uuid = $1`,
			[uuId])).rows[0].num_of_players;
            for (let i = 0; i < numOfPlayers; i++) {
                const user = (
                    await client.query(/* sql */ `select users.name from rooms left outer join user_room_mapping
					on rooms.id = user_room_mapping.room_id
                	left outer join users on users.id = user_room_mapping.user_id where  uuid = $1 and player_order = $2`,
					[uuId, i]
                    )
                ).rows[0].name;
                await client.query(/*sql*/`update users set is_in_room = false where name = $1`, [user]);
            }
            io.to(`${uuId}`).emit('save-game-ready', '');
        });

        // game recorder
        socket.on('gameRecord',(data) => {
            io.emit('gameRecordSend',data);
        });

        socket.on('disconnect',async() => {
            if ((socket.request as express.Request).session["uuId"] && (socket.request as express.Request)
			.session["user"].name) {
                const username = (socket.request as express.Request).session["user"].name;
                const uuId = (socket.request as express.Request).session["uuId"];
                const doesRoomExist = (await client.query(/* sql */`select from rooms where uuid = $1`,[uuId]))
				.rows[0];
                if(doesRoomExist){
                    const roomId = (await client.query(/*sql*/`select id from rooms where uuid = $1`,[uuId]))
					.rows[0].id;
					const result = (await client.query(/*sql*/`select id,is_in_room,is_host,is_ready from users
					where users.name = $1`,[username])).rows[0]
                    const numOfPlayers = (await client.query(/*sql*/`select num_of_players from rooms where uuid = $1`,
					[uuId])).rows[0].num_of_players;
                    const isStarted = (await client.query(/*sql*/`select is_started from rooms where uuid = $1`,
					[uuId])).rows[0].is_started;
                    const numOfReadyPlayers = (await client.query(/*sql*/`select num_of_ready_players from rooms
					where uuid = $1`, [uuId])).rows[0].num_of_ready_players;
                    // setTimeout(async ()=>{
                    if(numOfPlayers < 2 && result.isInRoom){
                        (socket.request as express.Request).session["uuId"] = 0;
                        (socket.request as express.Request).session.save();
                        await client.query(/*sql*/`delete from user_room_mapping where room_id = $1`, [roomId]);
                        await client.query(/*sql*/`delete from rooms where uuid = $1`, [uuId]);
                        await playerReset(username);
                    }else if(!isStarted){
                        (socket.request as express.Request).session["uuId"] = 0;
                        (socket.request as express.Request).session.save();
                        await client.query(/*sql*/`update rooms set num_of_players = num_of_players - 1
						where uuid = $1`, [uuId]);
                        await client.query(/*sql*/`delete from user_room_mapping where user_id = $1`,[result.userId]);
                        if(result.isHost){
                            const newHostId = (await client.query(/* sql */ `select user_id from user_room_mapping where
							room_id = $1`, [roomId])).rows[0].user_id;
                            const newHost = (await client.query(/* sql */ `select users.name from users where id = $1`,
							[newHostId])).rows[0].name;
                            await client.query(/*sql*/`update users set is_host = $1 where name = $2`,[true,newHost]);
                            io.in(newHost).emit('game-host-assigned');
                        }
                        if(result.isReady){
                            await client.query(/*sql*/`update rooms set num_of_ready_players = num_of_ready_players - 1
							where uuid = $1`, [uuId]);
                        }else if(numOfPlayers - 1 === numOfReadyPlayers && numOfPlayers - 1 > 1){
                            const host = (
                                await client.query(
                                    /* sql */ `select users.name from rooms left outer join user_room_mapping
									on rooms.id = user_room_mapping.room_id;
                                    left outer join users on users.id = user_room_mapping.user_id
									where is_host = $1 and uuid = $2`,[true,uuId]
                                )
                            ).rows[0].name;
                            io.in(host).emit('you-may-start');
                        }
                        await playerReset(username);
                        const RoomData = await allRoomData();
                        const joiningRoomData = JSON.parse(RoomData).filter((item: any) => item.uuid == uuId);
						// change from find to filter
                        io.to(uuId).emit('quit-room', joiningRoomData);
                    }
                    const roomData = await allRoomData();
                    io.emit('room-information', roomData);
                    socket.leave(uuId);
                    // },1000);
                }
            }
        })
    }

    socket.on('sound-global',async(data) => {
        await client.query(/* sql */ `insert into chat_record(name,message,date_send) values ($1,$2,now())`,
		[(socket.request as express.Request).session["user"].name, {message: data}]);
        io.emit('sound-transverse-global', {user: (socket.request as express.Request).session["user"].name,message:data});
    })
    socket.on('sound-user',async(data) => {
        if ((socket.request as express.Request).session["loggedIn"]) {
            await client.query(/* sql */ `insert into chat_record_user(name,message,date_send)
			values ($1,$2,now())`, [(socket.request as express.Request).session["user"].name, {message: data}]);
        }
        io.to('user').emit('sound-transverse-user', {user: (socket.request as express.Request)
		.session["user"].name, message: data});
    })
    socket.on('sound-room',async(data) => {
        const roomId = (await client.query(/* sql */ `SELECT id FROM rooms where uuid = $1`,
		[(socket.request as express.Request).session["uuId"]])).rows[0].id;
        await client.query(/* sql */ `insert into chat_record_room(name,message,room,date_send)
		values ($1,$2,$3,now())`,[
            (socket.request as express.Request).session["user"].name,
            {message:data},
            roomId,
        ])
        const uuId = (socket.request as express.Request).session["uuId"];
        io.to(`${uuId}`).emit('sound-transverse-room', {user:(socket.request as express.Request).session["user"].name,
		message:data});
    })
    socket.on('sound-friend',async(data) => {
        io.to(`friend-${(socket.request as express.Request).session["user"].name}`)
		.emit('sound-transverse-friend',{user:(socket.request as express.Request).session["user"].name,message:data});
    })
})

export const roomService = new RoomService(client);
export const roomController = new RoomController(roomService);
export const userService = new UserService(client);
export const userController = new UserController(userService)
export const historyService = new HistoryService(client);
export const historyController = new HistoryController(historyService);
export const messageService = new MessageService(client);
export const messageController = new MessageController(messageService)
// export const soundService = new SoundService(client);
export const soundController = new SoundController()
export const friendService = new FriendService(client);
export const friendController = new FriendController(friendService);
export const adminService = new AdminService(client);
export const adminController = new AdminController(adminService)

initRoutes();
app.use("/",roomRoutes);
app.use("/",userRoutes);
app.use("/",historyRoutes);
app.use("/",messageRoutes)
app.use("/",soundRoutes)
app.use("/",friendRoutes)
app.use("/",adminRoutes)

export function randomArray(n:number){
    const array:number[] = [];
    while(array.length < n){
        const random = Math.floor(Math.random() * n);
        if(array.includes(random)){
            continue;
        }else{
            array.push(random);
        }
    }
    return array;
}

export async function uuidGenerator():Promise<any>{
    let id = uuid();
    const uuidArray = (await client.query(/*sql*/`select uuid from rooms`)).rows
    if(uuidArray[0]){
        for(let obj of uuidArray){
            if(obj.uuid === id){
                return uuidGenerator();
            }else{
                return id;
            }
        }
    }else{
        return id;
    }
}

export async function playerReset(username:string) {
    await client.query(/*sql*/`update users set is_in_room = false where users.name = $1`, [username]);
    await client.query(/*sql*/`update users set is_ready = false where name = $1`, [username]);
    await client.query(/*sql*/`update users set is_playing = false where name = $1`, [username]);
    await client.query(/*sql*/`update users set is_host = false where name = $1`, [username]);
    await client.query(/*sql*/`update users set player_order = 0 where name = $1`, [username]);
}

export async function determineNextPlayer(uuId: string, currentTurn: number, numOfPlayers: number) {
    const whichPlayer = currentTurn % numOfPlayers;
    await client.query(/*sql*/`update rooms set which_player = $1 where uuid = $2`,[whichPlayer,uuId]);
    await client.query(/*sql*/`update rooms set current_turn = current_turn + 1 where uuid = $1`,[uuId]);
    return whichPlayer;
}

export async function allRoomData(){
    const roomData = (await client.query(/*sql*/`select rooms.*, users.name as username from rooms
        left outer join user_room_mapping on rooms.id = user_room_mapping.room_id
        left outer join users on users.id = user_room_mapping.user_id where num_of_players > 0 and
		is_ended = false and users.name is not null`)
    ).rows;
	// console.log(roomData)
    return JSON.stringify(roomData);
}

export async function determineFirstPlayer(uuId:string,numOfPlayers:number){
    const array = randomArray(numOfPlayers);
    for(let i = 0; i < numOfPlayers; i++){
        const username = (await client.query(/*sql*/`select users.name from rooms
			left outer join user_room_mapping on rooms.id = user_room_mapping.room_id
			left outer join users on users.id = user_room_mapping.user_id
			where uuid = $1`,[uuId]
		)).rows[i].name;
        await client.query(/*sql*/`update users set player_order = $1 where name = $2`,[array[i],username]);
    }
    return (await client.query(/*sql*/`select users.name from rooms
		left outer join user_room_mapping on rooms.id = user_room_mapping.room_id
		left outer join users on users.id = user_room_mapping.user_id
		where uuid = $1 and player_order = 0`,[uuId]
	)).rows[0].name;
}

export async function endTurn(uuId:string,currentTurn:number,originalNumOfPlayers:number,quitPlayerArray:number[]){
    let whichPlayer = await determineNextPlayer(uuId,currentTurn,originalNumOfPlayers);
    let loopCount = 0;
    while (quitPlayerArray.includes(whichPlayer)){
        loopCount++;
        whichPlayer = await determineNextPlayer(uuId,currentTurn + loopCount,originalNumOfPlayers);
    }
    const nextPlayerName = (await client.query(/*sql*/`select users.name from rooms left outer join user_room_mapping
		on rooms.id = user_room_mapping.room_id
		left outer join users on users.id = user_room_mapping.user_id where player_order = $1 and uuid = $2`,
		[whichPlayer,uuId]
	)).rows[0].name;
	// console.log(nextPlayerName)
    io.in(uuId).emit("current-turn",currentTurn + 1 + loopCount);
    io.in(nextPlayerName).emit("your-turn",currentTurn + 1 + loopCount);
}

export async function reConnection(username:string,uuId:string){
    const numOfPlayers = (await client.query(/*sql*/`select num_of_players from rooms where uuid = $1`,[uuId]))
	.rows[0].num_of_players;
    const currentTurn = (await client.query(/*sql*/`select current_turn from rooms where uuid = $1`,
	[uuId])).rows[0].current_turn;
    const transmission = (await client.query(/*sql*/`SELECT transmission FROM rooms WHERE uuid = $1`,
	[uuId])).rows[0].transmission;
    const playerOrderArray = (await client.query(/*sql*/`select playerarray from rooms where uuid = $1`,
	[uuId])).rows[0].playerarray;
    const playerOrder = (await client.query(/*sql*/`select player_order from users where name = $1`,
	[username])).rows[0].player_order;
    const whichPlayer = (await client.query(/*sql*/`select which_player from rooms where uuid = $1`,
	[uuId])).rows[0].which_player;

    setTimeout(() => {
        io.to(username).emit("reconnect","you are the reconnected player");
        io.to(username).emit("playerOrder",playerOrder);
        io.to(username).emit("game-started",{"numOfPlayers":numOfPlayers,"currentPlayer":currentTurn,
		"playerOrderArray":playerOrderArray}); // current turn no + 1
        io.to(username).emit("transmission-received", JSON.parse(transmission));
        if (playerOrder === whichPlayer){ // determine the reconnect player is the current player no not
            io.to(username).emit("your-turn", currentTurn);
        }
    },1000)
}

app.use(express.static("public"));
app.use(express.static("secure"));
app.use(express.static("private"));

app.use((req:Request,res:Response) => {
    res.sendFile(path.resolve('./public/404.html'));
})

server.listen(8080,() => {
    console.log("Listening at https://localhost:8080");
})